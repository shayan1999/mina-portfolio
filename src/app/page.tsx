import { ReactElement, lazy } from "react";
export default function Home() {
  const Avatar = lazy(() => import("../assets/svgs/avatar.svg"));
  const Telegram = lazy(() => import("../assets/svgs/telegram.svg"));
  const Linkedin = lazy(() => import("../assets/svgs/linkedin.svg"));
  const Behance = lazy(() => import("../assets/svgs/behance.svg"));
  const Dribble = lazy(() => import("../assets/svgs/dribble.svg"));
  const ArrowRight = lazy(() => import("../assets/svgs/arrow-right.svg"));
  const ArrowDown = lazy(() => import("../assets/svgs/arrow-down-to-line.svg"));

  const SocialMedia = ({
    Icon,
    small,
  }: {
    Icon: ReactElement<any, any>;
    small?: boolean;
  }) => {
    return (
      <div
        className={`${
          small ? "p-1" : "p-2"
        } rounded-[50%] cursor-pointer transition-[0.5s] hover:opacity-[70%] border-[#E4E7E9] border-[1px]`}
      >
        {Icon}
      </div>
    );
  };
  return (
    <main className="flex min-h-screen bg-white flex-col items-center p-8 px-4 md:px-16">
      <h2 className="text-[#070707] pb-8 font-[500] text-[24px]">
        Mina’s <span className="text-[#9BA1A0]">Portfolio</span>
      </h2>
      <div className="border-[#E6ECF5] border-[1px] w-[100%] p-8 rounded-[24px] flex flex-col items-center">
        <Avatar />
        <h3 className="mt-8 font-[500] max-w-[720px] text-[#9BA1A0] text-[48px] text-center">
          I’m Mina
          <span className="text-[#070707]">, a Product Designer based</span> in
          Iran.
        </h3>
        <h5 className="text-[#9BA1A0] font-[400] mt-8 text-[16px] text-center max-w-[720px]">
          I create user-friendly products that solve real-world problems. I turn
          complex ideas into simple designs and work with teams to bring
          innovative solutions to life. I’m passionate about making a real
          impact.
        </h5>
        <div className="flex gap-6 mt-8 flex-row w-100 items-center max-w-[720px] flex-wrap">
          <SocialMedia Icon={<Linkedin />} />
          <SocialMedia Icon={<Behance />} />
          <SocialMedia Icon={<Telegram />} small />
          <SocialMedia Icon={<Dribble />} />
        </div>
      </div>
      <div className="mt-4 rounded-[60px] hover:opacity-[70%] cursor-pointer transition-[0.5s] gap-4 p-8 border-[#E6ECF5] border-[1px] w-[100%] flex justify-center items-center flex-row">
        <span className="text-[#9BA1A0] font-[400] text-[16px]">
          Get in touch
        </span>
        <div className="rounded-[20px] px-3 py-1 border-[#AAA7A7] border-[1px]">
          <ArrowRight />
        </div>
      </div>
      <div className="mt-4 flex gap-4 md:flex-row flex-col w-[100%]">
        <div className="md:flex-[2] flex-1 border-[#E6ECF5] p-12 pb-6 border-[1px] rounded-[24px] flex flex-col">
          <h3 className="font-[500] text-[28px] text-[#070707]">Experience</h3>
          <ul className="list-disc px-6 mt-4">
            <li className="font-[500] text-[28px] text-[#9BA1A0]">
              Senior FinTech Product Designer Wallex exchange, May 2022 - Jun
              2024
            </li>
            <li className="font-[500] text-[28px] text-[#9BA1A0]">
              UI Designer & Product Designer Yekta ertebat, January 2019 - April
              2022
            </li>
          </ul>
          <div className="w-[100%] flex flex-row justify-end mt-8">
            <button className="bg-[#000000] flex items-center gap-1 flex-row text-[#FFFFFF] font-[400] text-[20px] px-6 py-1 rounded-[20px]">
              {/* @ts-ignore */}
              resume <ArrowDown width={25} height={25} />
            </button>
          </div>
        </div>
        <div className="flex-1 border-[#E6ECF5] p-12 pb-6 border-[1px] rounded-[24px] flex flex-col">
          <h3 className="font-[500] text-[28px] text-[#070707]">Education</h3>
          <span className="mt-4 font-[500] text-[28px] text-[#9BA1A0]">
            English Language and Literature, General
          </span>
          <span className="font-[500] text-[28px] text-[#9BA1A0]">
            Bachelor of Arts - BA Shahid Bahonar University of Kerman{" "}
          </span>
          <span className="font-[500] text-[28px] text-[#9BA1A0]">
            2014-2017
          </span>
        </div>
      </div>
      <div className="w-[100%] flex flex-row justify-between items-center mt-16">
        <div className="flex-1">
          <h2 className="hidden md:block text-[#070707] font-[500] text-[24px]">
            Mina <span className="text-[#9BA1A0]">Saeidyan</span>
          </h2>
        </div>
        <div className="flex flex-row gap-12 items-center justify-center">
          <span className="text-[#BDBDBD] transition-[0.5s] cursor-pointer hover:opacity-[70%] font-[500] text-[16px]">
            Home
          </span>
          <span className="text-[#BDBDBD] transition-[0.5s] cursor-pointer hover:opacity-[70%] font-[500] text-[16px]">
            About
          </span>
          <span className="text-[#BDBDBD] transition-[0.5s] cursor-pointer hover:opacity-[70%] font-[500] text-[16px]">
            Contact
          </span>
        </div>
        <div className="hidden md:block flex-1" />
      </div>
    </main>
  );
}
